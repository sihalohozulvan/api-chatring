<?php
    header('Content-type: application/json');
    include('../config/conection_db.php');
    $rekening = mysqli_query($koneksi,"select * from tbl_rekening");
    $response = array();
    if(mysqli_num_rows($rekening) > 0 ){
        while($data = mysqli_fetch_array($rekening)){
            $h['id'] = $data["id"];
            $h['nama'] = $data["nama"];
            $h['nama_bank'] = $data["nama_bank"];
            $h['nomor_rekening'] = $data["nomor_rekening"];
            array_push($response, $h);
        }
        $response = ['status' => true,
                   'message' => 'Berhasil mengambil data rekening',
                   'result' => $response];
        
    }
    else {
        $response = ['status' => false,
                    'message' => 'Gagal mengambil data rekening',
                    'result' => null];
    }
    echo json_encode($response);
?>