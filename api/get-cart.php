<?php
    header('Content-type: application/json');
    include('../config/conection_db.php');
    $userId = (int)$_GET['user_id'];
    $query_cart="select * from tbl_cart where status = 1 and customer_id=".$userId;
    $dataCart=mysqli_query($koneksi,$query_cart);
    
    $response = array();
    if(mysqli_num_rows($dataCart) > 0 ){
        while($data=(mysqli_fetch_array($dataCart)))
        {
            if($data['promo_id'] != null){
                $qry=mysqli_query($koneksi,"select * from tbl_promo where id=".$data['promo_id']);
                while ($promo=mysqli_fetch_array($qry)) {
                    $kodePromo = $promo['kode_promo'];
                }
            }else{
                $kodePromo = null;
            }
            $cartId = $data['id'];
            $h['status'] = true;
            $h['promo_id'] = $data['promo_id'];
            $h['kode_promo'] = $kodePromo ;
            $h['diskon'] = $data['diskon'];
            $h['count_qty'] = $data["count_qty"];
            $h['subtotal'] = $data["subtotal"];
            $h['grand_total'] = $data["grand_total"];
        }
        $qry = mysqli_query($koneksi,"select * from tbl_cart_items where cart_id = $cartId");
        if(mysqli_num_rows($qry) < 1 ){        
            $response = ['status' => false,
            'message' => 'Data cart kosong',
            'result' => null];
            echo json_encode($response);
            die;
        }
        $temp = [];
                while ($cart=mysqli_fetch_array($qry)) {
                    $data = [
                        "id" => $cart['id'],
                        "produk_id" => $cart['produk_id'],
                        "nama_produk" => $cart['nama_produk'],
                        "qty" => $cart['qty'],
                        "harga" => $cart['harga'],
                        "total_harga" => $cart['total_harga'],
                        "cart_id" => $cart['cart_id'],
                        "status" => $cart['status'],
                        "gambar" => "http://$_SERVER[HTTP_HOST]/img/".$cart['gambar'],
                        "created_at" => $cart['created_at'],
                        "updated_at" => $cart['updated_at']
                    ];
                    array_push($temp, $data);
            }
           
            $h['cart_items'] = $temp;
        

        $response = ['status' => true,
                   'message' => 'Berhasil mengambil data Cart',
                   'result' => $h];
        
    }
    else {
        $response = ['status' => false,
                    'message' => 'Gagal mengambil data Cart',
                    'result' => null];
    }
    echo json_encode($response);
?>