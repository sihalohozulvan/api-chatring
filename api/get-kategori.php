<?php
    header('Content-type: application/json');
    include('../config/conection_db.php');
    $kategori = mysqli_query($koneksi,"select * from tbl_kategori");
    $response = array();
    if(mysqli_num_rows($kategori) > 0 ){
        while($data = mysqli_fetch_array($kategori)){
            $h['id'] = $data["id"];
            $h['kategori'] = $data["nama_kategori"];
            $h['slug'] = $data["slug"];
            array_push($response, $h);
        }
        $response = ['status' => true,
                   'message' => 'Berhasil mengambil data kategori',
                   'result' => $response];
        
    }
    else {
        $response = ['status' => false,
                    'message' => 'Gagal mengambil data kategori',
                    'result' => null];
    }
    echo json_encode($response);
?>