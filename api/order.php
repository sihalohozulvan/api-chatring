<?php 

    //CREATE ORDER

    error_reporting(0);
    header('Content-type: application/json');
    include('../config/conection_db.php');
    $userId = (int)$_POST['user_id'];
    $invoice = 'INV-'.$userId.'-'.uniqid();
    $create = date('Y-m-d H:i:s');
    $dataCart = getCartCustomer($userId);
    $diskon = $dataCart['diskon'] == null ? '' : $dataCart['diskon'];
    
    if($dataCart['diskon'] == null){
        $kategori = mysqli_query($koneksi,"insert into tbl_order (invoice,count_qty,
        subtotal,grand_total,customer_id,status,rekening_id,created_at,updated_at) values ('".$invoice."',
        '".$dataCart['count_qty']."',
        '".$dataCart['subtotal']."',
        '".$dataCart['grand_total']."',
        ".$userId.",1,
        ".$_POST['paymethod'].",
        '$create',
        '$create')");
        
    }
    else{
        $kategori = mysqli_query($koneksi,"insert into tbl_order (invoice,promo_id,diskon,count_qty,
        subtotal,grand_total,customer_id,status,rekening_id,created_at,updated_at) values ('".$invoice."',
        '".$dataCart['promo_id']."',
        '".$dataCart['diskon']."',
        '".$dataCart['count_qty']."',
        '".$dataCart['subtotal']."',
        '".$dataCart['grand_total']."',
        ".$_SESSION['id'].",1,
        ".$_POST['paymethod'].",
        '$create',
        '$create')");
    }

    $dataOrder = getOrderCustomer($userId);
    $q="select * from tbl_cart_items where cart_id = ".$dataCart['id'];
    $cartItems=mysqli_query($koneksi,$q);
    while($data=(mysqli_fetch_array($cartItems)))
    {
        $dataOrderItem = mysqli_query($koneksi,"insert into tbl_order_items 
                                    (produk_id,nama_produk,qty,harga,total_harga,order_id,gambar)
                                     values(".$data['produk_id'].",
                                    '".$data['nama_produk']."',
                                    ".$data['qty'].",
                                    ".$data['harga'].",
                                    ".$data['total_harga'].",
                                    ".$dataOrder.",
                                    '".$data['gambar']."')");
    }

    $dataOrder = [];
    $select_query="select * from tbl_order where customer_id = ".$userId." ORDER BY id DESC limit 1";
    $run_query=mysqli_query($koneksi,$select_query);
    while($row=(mysqli_fetch_array($run_query)))
    {	
        if($row['promo_id'] != null){
            $qry=mysqli_query($koneksi,"select * from tbl_promo where id=".$data['promo_id']);
            while ($promo=mysqli_fetch_array($qry)) {
                $kodePromo = $promo['kode_promo'];
            }
        }else{
            $kodePromo = null;
        }
        $status = $row['status'];
        if($status ==  1){
            $status = "Pending";
        }elseif($status ==  2){
            $status = "Diproses";
        }elseif($status ==  3){
            $status = "Dikonfirmasi";
        }elseif($status ==  4){
            $status = "Dipacking";
        }elseif($status ==  5){
            $status = "Diantar";
        }elseif($status ==  6){
            $status = "Diterima";
        }elseif($status ==  7){
            $status = "Ditolak";
        }

        $temp['id'] = $row['id'];
        $temp['invoice'] = $row['invoice'];
        $temp['promo_id'] = $row['promo_id'];
        $temp['kode_promo'] = $kodePromo;
        $temp['diskon'] = $row['diskon'];
        $temp['count_qty'] = $row['count_qty'];
        $temp['subtotal'] = $row['subtotal'];
        $temp['grand_total'] = $row['grand_total'];
        $temp['status'] = $status;
        $temp['created_at'] = date('d M Y', strtotime($row['created_at']));
        
        $tempOrderItems = [];
        $query="select * from tbl_order_items where order_id= ".$row['id']." ORDER BY id DESC";
        $orderItems=mysqli_query($koneksi,$query);
        while($dataOrderItems=(mysqli_fetch_array($orderItems)))
        {	
            $data = [
                "id" => $dataOrderItems['id'],
                "produk_id" => $dataOrderItems['produk_id'],
                "nama_produk" => $dataOrderItems['nama_produk'],
                "qty" => $dataOrderItems['qty'],
                "harga" => $dataOrderItems['harga'],
                "total_harga" => $dataOrderItems['total_harga'],
                "order_id" => $dataOrderItems['order_id'],
                "gambar" => "http://$_SERVER[HTTP_HOST]/img/".$dataOrderItems['gambar'],
            ];
            array_push($tempOrderItems, $data);
          
        }
        $temp['orderItems'] =  $tempOrderItems;

        $rekening = mysqli_query($koneksi,"select * from tbl_rekening where id = ".$row['rekening_id']);
        while($data = mysqli_fetch_array($rekening)){
            $h = [
                'id' => $data["id"],
                'nama' => $data["nama"],
                'nama_bank' => $data["nama_bank"],
                'nomor_rekening' => $data["nomor_rekening"]
            ];
        }
        $temp['rekening'] = $h;
    }



         
    $updateCart = mysqli_query($koneksi, "update tbl_cart set status = 0 WHERE id = ".$dataCart['id']);
    if($updateCart){
        $response = ['status' => True,
                    'message' => 'Pesanan Kamu Berhasil Dibuat',
                    'result' => $temp];
        echo json_encode($response);
        die;
    }
    else{
        $response = ['status' => True,
                    'message' => 'Pesanan Kamu Tidak Berhasil Dibuat',
                    'result' => null];
        echo json_encode($response);
        die;
    }

    function getCartCustomer($userId){
        include('../config/conection_db.php');
        $query_cart="select * from tbl_cart where status = 1 and customer_id=".$userId;
        $dataCart=mysqli_query($koneksi,$query_cart);
        while($data=(mysqli_fetch_array($dataCart)))
        {
        // var_dump('sdf');die;
            $dataCart = [
                'id' => $data['id'],
                'promo_id' => $data['promo_id'],
                'diskon' => $data['diskon'],
                'count_qty' => $data['count_qty'],
                'subtotal' => $data['subtotal'],
                'grand_total' => $data['grand_total'],
                'customer_id' => $data['customer_id'],
                'status' => $data['status']
            ];
        }
        return $dataCart;
    }


    function getOrderCustomer($userId){
        
        include('../config/conection_db.php');
        $q="select * from tbl_order where status = 1 and customer_id=".$userId." ORDER BY id DESC limit 1";
        $dataOrder=mysqli_query($koneksi,$q);
        while($data=(mysqli_fetch_array($dataOrder)))
        {
            $orderId = $data['id'];
        }
        return $orderId;
    }

?>