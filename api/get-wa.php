<?php
    header('Content-type: application/json');
    include('../config/conection_db.php');
    $kategori = mysqli_query($koneksi,"select * from tbl_wa limit 1");
    $response = array();
    if(mysqli_num_rows($kategori) > 0 ){
        while($data = mysqli_fetch_array($kategori)){
            $h['id'] = $data["id"];
            $h['no_wa'] = $data["no_wa"];
        }
        $response = ['status' => true,
                   'message' => 'Berhasil mengambil data Chat Whatsapp',
                   'result' => $h];
        
    }
    else {
        $response = ['status' => false,
                    'message' => 'Gagal mengambil data Chat Whatsapp',
                    'result' => null];
    }
    echo json_encode($response);
?>