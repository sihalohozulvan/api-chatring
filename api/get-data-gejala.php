<?php
    header('Content-type: application/json');
    include('../../config/conection_db.php');

    $penyakit = mysqli_query($koneksi,"select * from gejala");
    if(mysqli_num_rows($penyakit) > 0 ){

        $response = array();
        $response = ['status' => true,
                        'message' => 'Berhasil Mengambil data penyakit'];
        $response["result"] = array();
        while($data = mysqli_fetch_array($penyakit)){
            $h['id'] = $data["id"];
            $h['nama'] = $data["nama_gejala"];
            array_push($response["result"], $h);
        }
        
    }
    else {
        $response = ['status' => false,
        'message' => 'Gagal mengambil data'];
    }
    echo json_encode($response);
?>