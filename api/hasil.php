<?php
    // header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json");
    header("Access-Control-Allow-Methods: POST");
    $json = file_get_contents('php://input');
    include('../../config/conection_db.php');
    $temp = json_decode($json, true);
    $sumSimilarity = 0;
    $tempHasil = array();
    $penyakit = mysqli_query($koneksi,"select * from penyakit");
    $string = '';
    $tempPerhitunganSumSimiliarity ='';
    $count = mysqli_num_rows($penyakit);
    while($row = mysqli_fetch_array($penyakit))
    {    
        $data = ['id' => $row['id'],
                 'nama_penyakit' => $row['nama_penyakit']
                ]; 
        $data['nilai'] = array();
        
        $sumWeight = 0;
        $sumSimilarityWeigh = 0;
        foreach($temp['data'] as $key => $value){
                $bobot = mysqli_query($koneksi,"select * from bobot where
                penyakit_id = '".$row['id']."' and gejala_id = '".$value['id_gejala']."'");
                if(mysqli_num_rows($bobot) > 0 ){
                    while($databobot = mysqli_fetch_array($bobot))
                    { 
                        $tempNilai['nilai_bobot']= $databobot['bobot'];
                        
                    }
                }
                
                
                $similiar = mysqli_query($koneksi,"select * from similiar where
                penyakit_id = '".$row['id']."' and gejala_id= '".$value['id_gejala']."'");
                if(mysqli_num_rows($similiar) > 0 ){ 
                    while($datasimiliar = mysqli_fetch_array($similiar))
                    { 
                        if($value['answer'] == 1){
                            $tempNilai['nilai_similiar'] = 1;
                        }
                        else{
                            $tempNilai['nilai_similiar'] = 0;
                        }
                    }
                }
               
                if(!empty($tempNilai)){
                    //Rumus untuk mencari 𝑤1 + 𝑤2 + ⋯ + 𝑤n
                    $sumWeight += $tempNilai['nilai_bobot'];
                    $string = $string.' + '.$tempNilai['nilai_bobot'];

                    //Rumus untuk mencari 𝑠1𝑥𝑤1 + 𝑠2𝑥𝑤2 + ⋯ + 𝑠𝑛𝑥𝑤n
                    $sumSimilarityWeigh += $tempNilai['nilai_bobot'] * $tempNilai['nilai_similiar'];

                    // Hasil 𝑠1𝑥𝑤1 + 𝑠2𝑥𝑤2 + ⋯ + 𝑠𝑛𝑥𝑤 di simpan ke dalam array 
                    array_push($data['nilai'], $tempNilai);
                    $tempNilai = null;
                }
            }
                if(count($data['nilai']) > 0){
                    
                    if($sumSimilarityWeigh != 0 && $sumWeight != 0){

                        //Rumus untuk mencari 𝑠𝑖𝑚𝑖𝑙𝑎𝑟𝑖𝑡𝑦 (𝑇, 𝑆)
                        $data['similarity'] = ($sumSimilarityWeigh / $sumWeight) * 100;
                        $sumSimilarity += $data['similarity'];
                    }
                    else{

                        $data['similarity'] = 0;
                        $sumSimilarity += $data['similarity'];
                    }
                    array_push($tempHasil, $data);
                }
        if($count%2==0){
            if($count == 1){
                $tempPerhitunganSumSimiliarity .=  number_format($sumSimilarity,2);
            }
            else{
                $tempPerhitunganSumSimiliarity .=  number_format($sumSimilarity,2).'+';
            }
        }    
        else{
            if($count == 1){
                $tempPerhitunganSumSimiliarity .=  number_format($sumSimilarity,2);
            }
            else{
                $tempPerhitunganSumSimiliarity .=  number_format($sumSimilarity,2).'+';
            }
        } 
        $count--;
        $data['nilai'] = [];
    }
    // echo $tempPerhitunganSumSimiliarity;
    $tempPerhitunganSimiliarity = [];
    $tempNormalisasi = [];
    $sw = '';
    $w = '';
    
    foreach($tempHasil as $key => $value){
        $sumSW = 0;
        $sumW = 0;
            $a = count($value['nilai']);
            foreach($value['nilai'] as $k => $val){
                $sumSW += $val['nilai_bobot'] * $val['nilai_similiar'];
                $sumW += $val['nilai_bobot'];
                    if($a%2 == 0){
                        if($a == 1){
                            $sw .= '('.$val['nilai_bobot'].'*'.$val['nilai_similiar'].')';
                            $w .= '('.$val['nilai_bobot'].')';
                        }else{
                            $sw .= '('.$val['nilai_bobot'].'*'.$val['nilai_similiar'].')+';
                            $w .= '('.$val['nilai_bobot'].')+';
                        }
                    }else{
                        if($a == 1){
                            $sw .= '('.$val['nilai_bobot'].'*'.$val['nilai_similiar'].')';
                            $w .= '('.$val['nilai_bobot'].')';
                        }else{
                            $sw .= '('.$val['nilai_bobot'].'*'.$val['nilai_similiar'].')+';
                            $w .= '('.$val['nilai_bobot'].')+';
                        }
                    }
                    $a--;
            }
            $hasilBagiSumSWandW = $sumSW / $sumW;
            $persenhasilBagiSumSWandW = $hasilBagiSumSWandW * 100;
            $tempSimiliar = 'Similiarity '.$value['nama_penyakit'].' = '.$sw.' / '.$w.' = '.$sumSW.' / '.$sumW.' = '.$hasilBagiSumSWandW.' * 100 = '.$persenhasilBagiSumSWandW;
            array_push($tempPerhitunganSimiliarity, $tempSimiliar);
            $normal = $value['similarity'] / $sumSimilarity;
            $tempNormal = 'Nommalisasi '.$value['nama_penyakit'].' = '.$value['similarity'].' / '.$tempPerhitunganSumSimiliarity.' = '.number_format($normal,2).' * 100 = '.number_format(($normal*100),2);
            array_push($tempNormalisasi, $tempNormal);
            $sw = null;
            $w = null;
    }
    // echo json_encode($tempPerhitunganSimiliarity);
    
    $hasil = array();
    $penyakit = mysqli_query($koneksi,"select * from penyakit");
    while($row = mysqli_fetch_array($penyakit))
    { 
        $data = ['id' => $row['id'],
                 'nama_penyakit' => $row['nama_penyakit']
                ]; 
        $data['nilai'] = array();
        foreach($tempHasil as $key => $value){
            if($value['id'] == $row['id']){
                //Jika index similarity ada 
                if(isset($value['similarity']) && $value['similarity'] != 0){

                    //Rumus untuk mencari normalisasi di simpan ke dalam variabel normalisasi
                    $normalisasi = ($value['similarity'] / $sumSimilarity) * 100;
                    $simp = $value['similarity'];
                }
                //Jika index similarity tidak ada
                else{
                    $normalisasi = 0;
                    $simp = 0;
                }
                $tempNilai['nilai_similarity'] = $simp;
                $tempNilai['nilai_normarlisasi'] = $normalisasi;
            }
        }

        
        array_push($data['nilai'], $tempNilai);
        $tempNilai = [];
        array_push($hasil, $data);
    }
    

   
    $temp = 0;

    //Perulangan untuk mencari hasil normalisasai yang terbesar
    foreach($hasil as $key => $value){
        if($temp < $value['nilai'][0]['nilai_normarlisasi']){
            $temp = $value['nilai'][0]['nilai_normarlisasi'];
            $id_penyakit = $value['id'];
            $normalisasi = $value['nilai'][0]['nilai_normarlisasi'];
        }
        
    }

    $penyakit = mysqli_query($koneksi,"select * from penyakit where id = '$id_penyakit'");
    if(mysqli_num_rows($penyakit) > 0 ){
        $response = ['status' => true,
                     'message' => 'Berhasil Mengambil Hasil'];
        $response["result"] = array();
        while($row = mysqli_fetch_array($penyakit))
        { 
            $data = [
                'nama_penyakit' => $row['nama_penyakit'],
                'hasil_normalisasi' => number_format($normalisasi,2).'%',
                'solusi'   => $row['solusi'],
                'images' => $row['gambar'],
                'perhitungan_similiar' =>  $tempPerhitunganSimiliarity,
                'perhitungan_normalisasi' => $tempNormalisasi,
            ];
            array_push($response["result"], $data);
        }
    }
    else{
        $response = ['status' => false,
        'message' => 'Gagal mengambil data Kusioner'];
    }
    echo json_encode($response);
?>