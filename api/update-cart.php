<?php
    header('Content-type: application/json');
    include('../config/conection_db.php');
    $cartIds = (int)$_POST['cart_items'];
    $qty = (int)$_POST['qty'];
    $select_query="SELECT tbl_produk.harga as harga_produk,
            tbl_cart_items.cart_id as cart_id
            FROM tbl_cart_items
            INNER JOIN tbl_produk
            ON tbl_cart_items.produk_id = tbl_produk.id where tbl_cart_items.id=".$cartIds;
            $dataRelasi=mysqli_query($koneksi,$select_query);
    if(mysqli_num_rows($dataRelasi) > 0 ){
            while($data=(mysqli_fetch_array($dataRelasi)))
            {	
				// var_dump($data);die;
                $totalHarga = $qty	* $data['harga_produk'];
                $cartId = $data['cart_id'];
            }
            $cart = mysqli_query($koneksi,"update tbl_cart_items set 
            qty = ".$qty.", total_harga = $totalHarga where id=".$cartIds);  

       
        $data = sumCartItems($cartId);
        // var_dump($data);die;
        $update = updateCart($data,$cartId);

        $response = ['status' => true,
                   'message' => 'Berhasil update data Cart'];
        
    }
    else {
        $response = ['status' => false,
                    'message' => 'Gagal update data Cart'];
    }
    echo json_encode($response);



    function sumCartItems($cartId){
        include('../config/conection_db.php');
        $query="select SUM(total_harga) as sum_total, SUM(qty) as sum_qty from tbl_cart_items where cart_id=$cartId";
        $result= mysqli_query($koneksi,$query);
        $data = mysqli_fetch_assoc($result);
        return $data;
    }

    function updateCart($data, $cartId, $promo = null){
        include('../config/conection_db.php');
        $promoId = isset($promo['id']) ? $promo['id'] : 'null';
        $diskon = isset($promo['percentage_discount']) ? ($data['sum_total'] * $promo['percentage_discount']) / 100 : 'null';
        $subTotal = $data['sum_total']; 
        $grandTotal = $diskon != 'null' ? $data['sum_total'] - $diskon : $data['sum_total'];
        
        $updateCart = mysqli_query($koneksi, "update tbl_cart set count_qty = ".$data['sum_qty'].",
        grand_total = $grandTotal,
        subtotal =  $subTotal,
        diskon = $diskon,
        promo_id = $promoId WHERE id = $cartId");
        
        // var_dump($updateCart);die;
        if($updateCart){
            return true;
        }
        return false;
    }
?>