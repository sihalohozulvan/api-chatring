<?php

    //GET ORDER ALL
    
    error_reporting(0);
    header('Content-type: application/json');
    include('../config/conection_db.php');
    
    $userId = (int)$_GET['user_id'];
    $dataOrder = [];
    $select_query="select * from tbl_order where customer_id = ".$userId." ORDER BY id DESC";
    $run_query=mysqli_query($koneksi,$select_query);
    while($row=(mysqli_fetch_array($run_query)))
    {	
        if($row['promo_id'] != null){
            $qry=mysqli_query($koneksi,"select * from tbl_promo where id=".$data['promo_id']);
            while ($promo=mysqli_fetch_array($qry)) {
                $kodePromo = $promo['kode_promo'];
            }
        }else{
            $kodePromo = null;
        }

        $status = $row['status'];
        if($status ==  1){
            $status = "Pending";
        }elseif($status ==  2){
            $status = "Diproses";
        }elseif($status ==  3){
            $status = "Dikonfirmasi";
        }elseif($status ==  4){
            $status = "Dipacking";
        }elseif($status ==  5){
            $status = "Diantar";
        }elseif($status ==  6){
            $status = "Diterima";
        }elseif($status ==  7){
            $status = "Ditolak";
        }
        $temp['id'] = $row['id'];
        $temp['invoice'] = $row['invoice'];
        $temp['promo_id'] = $row['promo_id'];
        $temp['kode_promo'] = $kodePromo;
        $temp['diskon'] = $row['diskon'];
        $temp['count_qty'] = $row['count_qty'];
        $temp['subtotal'] = $row['subtotal'];
        $temp['grand_total'] = $row['grand_total'];
        $temp['status'] = $status;
        $temp['created_at'] = date('d M Y', strtotime($row['created_at']));
        
        $tempOrderItems = [];
        $query="select * from tbl_order_items where order_id= ".$row['id']." ORDER BY id DESC";
        $orderItems=mysqli_query($koneksi,$query);
        while($dataOrderItems=(mysqli_fetch_array($orderItems)))
        {	
            $data = [
                "id" => $dataOrderItems['id'],
                "produk_id" => $dataOrderItems['produk_id'],
                "nama_produk" => $dataOrderItems['nama_produk'],
                "qty" => $dataOrderItems['qty'],
                "harga" => $dataOrderItems['harga'],
                "total_harga" => $dataOrderItems['total_harga'],
                "order_id" => $dataOrderItems['order_id'],
                "gambar" => "http://$_SERVER[HTTP_HOST]/img/".$dataOrderItems['gambar'],
            ];
            array_push($tempOrderItems, $data);
          
        }
        $temp['orderItems'] =  $tempOrderItems;
        array_push($dataOrder, $temp);
    }
    $response = ['status' => true,
        'message' => 'Berhasil mengambil data Order',
        'result' => $dataOrder];
    echo json_encode($response);
    die;
?>