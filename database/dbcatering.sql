-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 13, 2020 at 10:02 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbcatering`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `id` int(11) NOT NULL,
  `nama` varchar(500) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`id`, `nama`, `email`, `password`) VALUES
(1, 'Admin', 'admin@gmail.com', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cart`
--

CREATE TABLE `tbl_cart` (
  `id` int(11) NOT NULL,
  `promo_id` int(11) DEFAULT NULL,
  `diskon` int(11) DEFAULT NULL,
  `count_qty` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL,
  `grand_total` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_cart`
--

INSERT INTO `tbl_cart` (`id`, `promo_id`, `diskon`, `count_qty`, `subtotal`, `grand_total`, `customer_id`, `status`) VALUES
(98, NULL, NULL, 1, 154000, 154000, 1, 0),
(99, NULL, NULL, 2, 308000, 308000, 1, 0),
(100, 1, 15400, 1, 154000, 138600, 1, 0),
(101, 1, 5000, 1, 50000, 45000, 1, 0),
(102, 1, 10000, 1, 100000, 90000, 1, 0),
(103, 1, 15400, 1, 154000, 138600, 1, 0),
(104, NULL, NULL, 1, 50000, 50000, 1, 0),
(115, NULL, NULL, 0, 0, 0, 1, 0),
(130, NULL, NULL, 1, 50000, 50000, 1, 0),
(132, NULL, NULL, 1, 100000, 100000, 1, 0),
(133, NULL, NULL, 1, 100000, 100000, 1, 0),
(134, NULL, NULL, 1, 100000, 100000, 1, 0),
(135, NULL, NULL, 1, 100000, 100000, 1, 0),
(136, NULL, NULL, 1, 100000, 100000, 1, 0),
(137, NULL, NULL, 1, 100000, 100000, 1, 0),
(138, NULL, NULL, 1, 100000, 100000, 1, 0),
(139, NULL, NULL, 2, 200000, 200000, 1, 0),
(140, NULL, NULL, 3, 300000, 300000, 1, 0),
(141, NULL, NULL, 1, 100000, 100000, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cart_items`
--

CREATE TABLE `tbl_cart_items` (
  `id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `nama_produk` varchar(500) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` double(11,2) NOT NULL,
  `total_harga` int(11) NOT NULL,
  `cart_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `gambar` varchar(200) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_cart_items`
--

INSERT INTO `tbl_cart_items` (`id`, `produk_id`, `nama_produk`, `qty`, `harga`, `total_harga`, `cart_id`, `status`, `gambar`, `created_at`, `updated_at`) VALUES
(129, 13, 'Kemeja Bahan Sisiran', 1, 154000.00, 154000, 98, 1, 'bahan sisiran.jpeg', NULL, NULL),
(130, 13, 'Kemeja Bahan Sisiran', 2, 154000.00, 308000, 99, 1, 'bahan sisiran.jpeg', NULL, NULL),
(131, 13, 'Kemeja Bahan Sisiran', 1, 154000.00, 154000, 100, 1, 'bahan sisiran.jpeg', NULL, NULL),
(132, 14, 'Kemeja Bahan Ulos', 1, 50000.00, 50000, 101, 1, 'bahan ulos.jpeg', NULL, NULL),
(133, 10, 'Jas Pucca', 1, 100000.00, 100000, 102, 1, 'backgoround.jpg', NULL, NULL),
(134, 13, 'Kemeja Bahan Sisiran', 1, 154000.00, 154000, 103, 1, 'bahan sisiran.jpeg', NULL, NULL),
(135, 14, 'Kemeja Bahan Ulos', 1, 50000.00, 50000, 130, 1, 'bahan ulos.jpeg', NULL, NULL),
(140, 10, 'Jas Pucca', 1, 100000.00, 100000, 132, 1, 'backgoround.jpg', NULL, NULL),
(141, 10, 'Jas Pucca', 1, 100000.00, 100000, 133, 1, 'backgoround.jpg', NULL, NULL),
(142, 10, 'Jas Pucca', 1, 100000.00, 100000, 134, 1, 'backgoround.jpg', NULL, NULL),
(143, 10, 'Jas Pucca', 1, 100000.00, 100000, 135, 1, 'backgoround.jpg', NULL, NULL),
(144, 10, 'Jas Pucca', 1, 100000.00, 100000, 136, 1, 'backgoround.jpg', NULL, NULL),
(145, 10, 'Jas Pucca', 1, 100000.00, 100000, 137, 1, 'backgoround.jpg', NULL, NULL),
(146, 10, 'Jas Pucca', 1, 100000.00, 100000, 138, 1, 'backgoround.jpg', NULL, NULL),
(147, 10, 'Jas Pucca', 2, 100000.00, 200000, 139, 1, 'backgoround.jpg', NULL, NULL),
(148, 10, 'Jas Pucca', 3, 100000.00, 300000, 140, 1, 'backgoround.jpg', NULL, NULL),
(149, 10, 'Jas Pucca', 1, 100000.00, 100000, 141, 1, 'backgoround.jpg', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_customer`
--

CREATE TABLE `tbl_customer` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `nohp` varchar(12) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `alamat` longtext DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_customer`
--

INSERT INTO `tbl_customer` (`id`, `nama`, `email`, `nohp`, `password`, `alamat`, `created_at`) VALUES
(1, 'Zulvan', 'admin@gmail.com', '878753726', 'sayalupa12', 'Medan Tunt', '2019-02-02 18:03:41'),
(26, 'sdf eventory', 'sihalohogue@gmail.com', '0', '', 'sdf', '2020-06-17 12:08:47'),
(27, 'sadf sadf', 'sihalohozulvan@gmail.com', '0', '', 'asdf', '2020-06-17 12:11:57'),
(28, 'sdaf sadf', 'sihalohozulvan@gmail.com', '0', '', 'sdfa', '2020-06-18 06:03:40'),
(29, 'sdf eventory', 'sihalohogue@gmail.com', '0', '', 'sdaf', '2020-06-18 06:04:06'),
(30, 'sdfa sihaloho', 'sihalohogue@gmail.com', '0', '', 'sdf', '2020-06-18 06:06:25'),
(31, 'sadf eventory', 'sihalohogue@gmail.com', '0', '', 'sdf', '2020-06-18 06:06:45'),
(32, 'sadfsadf sadf', 'sihalohozulvddddddan@gmail.com', '0', '', 'sadf', '2020-06-18 17:32:46'),
(33, 'sadfasss ', 'sisdafsadfhalohozulvan@gmail.com', '0', '', 'sadf', '2020-06-18 17:33:27'),
(34, ' sadf', 'sisdafsadfhalohozulvan@gmail.com', '0', '', 'sadf', '2020-06-18 17:40:07'),
(35, 'coba sadf', 'sihalohasdasdasdozulvan@gmail.com', '12334', '', 'sadf', '2020-06-23 15:32:50'),
(36, 'coba COba', 'COba@gmail.com', '8787877', 'COba@gmail.com', 'COba@gmail.com', '2020-06-23 15:48:50'),
(37, 'Bisaaja Bisaaja', 'Bisaaja@gmail.com', '0', '', 'Bisaaja', '2020-06-23 15:52:33'),
(38, 'lagi lagi', 'lagi@gmai.com', '0', 'lagi@gmai.com', 'lagi@gmai.com', '2020-06-23 15:54:09'),
(39, 'sdf', 'budiada@gmail.com', '123456', 'sayalupa12', 'Medan', '2020-08-21 00:00:00'),
(40, 'sdf', 'budiada@gmail.com', '123456', 'sayalupa12', 'Medan', '2020-08-21 00:00:00'),
(41, 'sdf', 'budiada@gmail.com', '123456', 'sayalupa12', 'Medan', '2020-08-21 00:00:00'),
(42, 'sdf', 'budisada@gmail.com', '123456', 'sayalupa12', 'Medan', '2020-08-21 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kategori`
--

CREATE TABLE `tbl_kategori` (
  `id` int(11) NOT NULL,
  `nama_kategori` varchar(200) NOT NULL,
  `slug` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_kategori`
--

INSERT INTO `tbl_kategori` (`id`, `nama_kategori`, `slug`) VALUES
(7, 'Kemeja', 'kemeja'),
(8, 'Songket', 'songket');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order`
--

CREATE TABLE `tbl_order` (
  `id` int(11) NOT NULL,
  `invoice` varchar(300) NOT NULL,
  `promo_id` int(11) DEFAULT NULL,
  `diskon` int(11) DEFAULT NULL,
  `count_qty` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL,
  `grand_total` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `cart_id` int(11) NOT NULL,
  `rekening_id` int(11) NOT NULL,
  `gambar` varchar(500) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_order`
--

INSERT INTO `tbl_order` (`id`, `invoice`, `promo_id`, `diskon`, `count_qty`, `subtotal`, `grand_total`, `customer_id`, `status`, `cart_id`, `rekening_id`, `gambar`, `created_at`, `updated_at`) VALUES
(120, 'INV-1-5f79c38634898', 1, NULL, 1, 50000, 50000, 1, 1, 130, 1, '', '2020-10-04 14:43:50', '2020-10-04 14:43:50'),
(121, 'INV-1-5f79c39b1fafb', NULL, NULL, 1, 50000, 50000, 1, 1, 130, 1, '', '2020-10-04 14:44:11', '2020-10-04 14:44:11'),
(122, 'INV-1-5f79c3a70d00c', NULL, NULL, 1, 50000, 50000, 1, 1, 130, 1, '', '2020-10-04 14:44:23', '2020-10-04 14:44:23'),
(123, 'INV-1-5f79c3b5caf24', NULL, NULL, 1, 50000, 50000, 1, 1, 130, 1, '', '2020-10-04 14:44:37', '2020-10-04 14:44:37'),
(124, 'INV-1-5f79c5254fe5c', NULL, NULL, 1, 100000, 100000, 1, 1, 132, 1, '', '2020-10-04 14:50:45', '2020-10-04 14:50:45'),
(125, 'INV-1-5f7c7f6314783', NULL, NULL, 1, 100000, 100000, 1, 1, 133, 1, '', '2020-10-06 16:29:55', '2020-10-06 16:29:55'),
(126, 'INV-1-5f807f2f53c56', NULL, NULL, 1, 100000, 100000, 1, 1, 134, 1, '', '2020-10-09 17:18:07', '2020-10-09 17:18:07'),
(127, 'INV-1-5f807f6a3f389', NULL, NULL, 1, 100000, 100000, 1, 1, 135, 1, '', '2020-10-09 17:19:06', '2020-10-09 17:19:06'),
(128, 'INV-1-5f807f84d616d', NULL, NULL, 1, 100000, 100000, 1, 1, 136, 1, '', '2020-10-09 17:19:32', '2020-10-09 17:19:32'),
(129, 'INV-1-5f807fd57f4c1', NULL, NULL, 1, 100000, 100000, 1, 1, 137, 1, '', '2020-10-09 17:20:53', '2020-10-09 17:20:53'),
(130, 'INV-1-5f8081fbbd182', NULL, NULL, 1, 100000, 100000, 1, 1, 138, 1, '', '2020-10-09 17:30:03', '2020-10-09 17:30:03'),
(131, 'INV-1-5f808263cbec2', NULL, NULL, 1, 100000, 100000, 1, 1, 139, 1, '', '2020-10-09 17:31:47', '2020-10-09 17:31:47'),
(132, 'INV-1-5f8082c2d7c75', NULL, NULL, 2, 200000, 200000, 1, 1, 139, 1, '', '2020-10-09 17:33:22', '2020-10-09 17:33:22'),
(133, 'INV-1-5f8082e0a0d1a', NULL, NULL, 1, 100000, 100000, 1, 1, 140, 1, '', '2020-10-09 17:33:52', '2020-10-09 17:33:52'),
(134, 'INV-1-5f8082f10af7c', NULL, NULL, 2, 200000, 200000, 1, 7, 140, 1, 'kuponpelatihan.png', '2020-10-09 17:34:09', '2020-10-09 17:34:09'),
(135, 'INV-1-5f8082fcc2499', NULL, NULL, 3, 300000, 300000, 1, 1, 140, 1, 'kuponpelatihan.png', '2020-10-09 17:34:20', '2020-10-09 17:34:20'),
(136, 'INV-1-5f84614dd258b', NULL, NULL, 1, 100000, 100000, 1, 1, 141, 1, '', '2020-10-12 15:59:41', '2020-10-12 15:59:41');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order_items`
--

CREATE TABLE `tbl_order_items` (
  `id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `nama_produk` varchar(500) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `total_harga` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `gambar` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_order_items`
--

INSERT INTO `tbl_order_items` (`id`, `produk_id`, `nama_produk`, `qty`, `harga`, `total_harga`, `order_id`, `gambar`) VALUES
(93, 14, 'Kemeja Bahan Ulos', 1, 50000, 50000, 122, 'bahan ulos.jpeg'),
(94, 14, 'Kemeja Bahan Ulos', 1, 50000, 50000, 123, 'bahan ulos.jpeg'),
(95, 10, 'Jas Pucca', 1, 100000, 100000, 124, 'backgoround.jpg'),
(96, 10, 'Jas Pucca', 1, 100000, 100000, 125, 'backgoround.jpg'),
(97, 10, 'Jas Pucca', 1, 100000, 100000, 126, 'backgoround.jpg'),
(98, 10, 'Jas Pucca', 1, 100000, 100000, 127, 'backgoround.jpg'),
(99, 10, 'Jas Pucca', 1, 100000, 100000, 128, 'backgoround.jpg'),
(100, 10, 'Jas Pucca', 1, 100000, 100000, 129, 'backgoround.jpg'),
(101, 10, 'Jas Pucca', 1, 100000, 100000, 130, 'backgoround.jpg'),
(102, 10, 'Jas Pucca', 1, 100000, 100000, 131, 'backgoround.jpg'),
(103, 10, 'Jas Pucca', 2, 100000, 200000, 132, 'backgoround.jpg'),
(104, 10, 'Jas Pucca', 1, 100000, 100000, 133, 'backgoround.jpg'),
(105, 10, 'Jas Pucca', 2, 100000, 200000, 134, 'backgoround.jpg'),
(106, 10, 'Jas Pucca', 3, 100000, 300000, 135, 'backgoround.jpg'),
(107, 10, 'Jas Pucca', 1, 100000, 100000, 136, 'backgoround.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_produk`
--

CREATE TABLE `tbl_produk` (
  `id` int(11) NOT NULL,
  `nama_produk` varchar(500) NOT NULL,
  `kategori_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` double(11,2) NOT NULL,
  `gambar` varchar(200) NOT NULL,
  `deskripsi` varchar(10000) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_produk`
--

INSERT INTO `tbl_produk` (`id`, `nama_produk`, `kategori_id`, `qty`, `harga`, `gambar`, `deskripsi`, `created_at`, `updated_at`) VALUES
(10, 'Jas Pucca', 7, 10, 100000.00, 'backgoround.jpg', 'Jas Pucca', '0000-00-00 00:00:00', '2020-06-18 17:15:30'),
(11, 'Jas Sadum', 6, 10, 1500000.00, 'bahan jas sadum.jpeg', 'Jas Sadum', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'Kemeja Bahan Sisiran', 7, 11, 154000.00, 'bahan sisiran.jpeg', 'Kemeja Bahan Sisiran', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'Kemeja Bahan Ulos', 7, 5, 50000.00, 'bahan ulos.jpeg', 'Kemeja Bahan Ulos', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'Kemeja Bahan Katun', 7, 6, 560000.00, 'katun.jpeg', 'Kemeja Bahan Katun', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'Iccor Morror', 8, 690000, 582000.00, 'iccor morror.JPG', 'Iccor Morror', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 'Mangiring', 8, 25, 250000.00, 'mangiring.JPG', 'Mangiring', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_promo`
--

CREATE TABLE `tbl_promo` (
  `id` int(11) NOT NULL,
  `nama_promo` varchar(500) NOT NULL,
  `kode_promo` varchar(200) NOT NULL,
  `start_at` datetime NOT NULL,
  `end_at` datetime NOT NULL,
  `percentage_discount` int(11) NOT NULL,
  `min_order` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_promo`
--

INSERT INTO `tbl_promo` (`id`, `nama_promo`, `kode_promo`, `start_at`, `end_at`, `percentage_discount`, `min_order`, `created_at`, `updated_at`) VALUES
(1, 'Promo Weekend', 'WEEKEND01', '2020-06-27 00:00:00', '2020-09-27 00:00:00', 10, 50000, '2020-06-15 00:00:00', '2020-06-15 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_rekening`
--

CREATE TABLE `tbl_rekening` (
  `id` int(11) NOT NULL,
  `nama_bank` varchar(200) NOT NULL,
  `nomor_rekening` varchar(200) NOT NULL,
  `nama` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_rekening`
--

INSERT INTO `tbl_rekening` (`id`, `nama_bank`, `nomor_rekening`, `nama`) VALUES
(1, 'BANK BRI', '123456789', 'PT. TOKO BERSAMA'),
(2, 'BCA', '23474845', 'Budi'),
(3, 'Mandiri', '245352345', 'Salsa');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_slider`
--

CREATE TABLE `tbl_slider` (
  `id` int(11) NOT NULL,
  `judul_slider` varchar(500) NOT NULL,
  `sub_judul_slider` varchar(500) NOT NULL,
  `gambar` varchar(200) NOT NULL,
  `url` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_slider`
--

INSERT INTO `tbl_slider` (`id`, `judul_slider`, `sub_judul_slider`, `gambar`, `url`) VALUES
(1, 'Ulos Murah', 'Murah Kali Bah Ulos Ini, Yuk Beli Sekarang', 'bahan%20jas%20sadum.jpeg', 'asdfsadf'),
(6, 'sdaf', 'sdaf', 'ad.png', 'sdfa');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_used_promo`
--

CREATE TABLE `tbl_used_promo` (
  `id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `promo_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_cart`
--
ALTER TABLE `tbl_cart`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `promo_id` (`promo_id`),
  ADD KEY `promo_id_2` (`promo_id`),
  ADD KEY `customer_id_2` (`customer_id`);

--
-- Indexes for table `tbl_cart_items`
--
ALTER TABLE `tbl_cart_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cart_id` (`cart_id`),
  ADD KEY `produk_id` (`produk_id`),
  ADD KEY `produk_id_2` (`produk_id`);

--
-- Indexes for table `tbl_customer`
--
ALTER TABLE `tbl_customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_kategori`
--
ALTER TABLE `tbl_kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_order`
--
ALTER TABLE `tbl_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `promo_id` (`promo_id`,`customer_id`,`cart_id`,`rekening_id`),
  ADD KEY `rekening_id` (`rekening_id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `cart_id` (`cart_id`);

--
-- Indexes for table `tbl_order_items`
--
ALTER TABLE `tbl_order_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `produk_id` (`produk_id`);

--
-- Indexes for table `tbl_produk`
--
ALTER TABLE `tbl_produk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kategori_id` (`kategori_id`);

--
-- Indexes for table `tbl_promo`
--
ALTER TABLE `tbl_promo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_rekening`
--
ALTER TABLE `tbl_rekening`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_slider`
--
ALTER TABLE `tbl_slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_used_promo`
--
ALTER TABLE `tbl_used_promo`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_cart`
--
ALTER TABLE `tbl_cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=142;

--
-- AUTO_INCREMENT for table `tbl_cart_items`
--
ALTER TABLE `tbl_cart_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=150;

--
-- AUTO_INCREMENT for table `tbl_customer`
--
ALTER TABLE `tbl_customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `tbl_kategori`
--
ALTER TABLE `tbl_kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_order`
--
ALTER TABLE `tbl_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;

--
-- AUTO_INCREMENT for table `tbl_order_items`
--
ALTER TABLE `tbl_order_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `tbl_produk`
--
ALTER TABLE `tbl_produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `tbl_promo`
--
ALTER TABLE `tbl_promo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_rekening`
--
ALTER TABLE `tbl_rekening`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_slider`
--
ALTER TABLE `tbl_slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_used_promo`
--
ALTER TABLE `tbl_used_promo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_cart`
--
ALTER TABLE `tbl_cart`
  ADD CONSTRAINT `tbl_cart_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `tbl_customer` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tbl_cart_ibfk_2` FOREIGN KEY (`promo_id`) REFERENCES `tbl_promo` (`id`) ON DELETE NO ACTION;

--
-- Constraints for table `tbl_cart_items`
--
ALTER TABLE `tbl_cart_items`
  ADD CONSTRAINT `tbl_cart_items_ibfk_1` FOREIGN KEY (`cart_id`) REFERENCES `tbl_cart` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tbl_order`
--
ALTER TABLE `tbl_order`
  ADD CONSTRAINT `tbl_order_ibfk_1` FOREIGN KEY (`rekening_id`) REFERENCES `tbl_rekening` (`id`) ON DELETE NO ACTION,
  ADD CONSTRAINT `tbl_order_ibfk_2` FOREIGN KEY (`promo_id`) REFERENCES `tbl_promo` (`id`) ON DELETE NO ACTION,
  ADD CONSTRAINT `tbl_order_ibfk_3` FOREIGN KEY (`customer_id`) REFERENCES `tbl_customer` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tbl_order_ibfk_4` FOREIGN KEY (`cart_id`) REFERENCES `tbl_cart` (`id`) ON DELETE NO ACTION;

--
-- Constraints for table `tbl_order_items`
--
ALTER TABLE `tbl_order_items`
  ADD CONSTRAINT `tbl_order_items_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `tbl_order` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tbl_order_items_ibfk_2` FOREIGN KEY (`produk_id`) REFERENCES `tbl_produk` (`id`) ON DELETE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
